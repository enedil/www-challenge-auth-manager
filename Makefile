#WARNINGS 	= -Weverything -Wno-reserved-id-macro -Wno-format-nonliteral
WARNINGS 	= -Wall
#CC 			= clang
CC			= gcc
CFLAGS 		= $(WARNINGS) -std=c11 -fsanitize=address,undefined
TARGET  	= runner
LDFLAGS 	= -lhiredis -lcrypto

$(TARGET): $(TARGET).c

clean:
	rm -f $(TARGET)

.PHONY: all clean
