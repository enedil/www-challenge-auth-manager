#define _GNU_SOURCE
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <hiredis/hiredis.h>
#include <openssl/sha.h>

#define SIZE 20

#ifndef REDIS_IP
#define REDIS_IP "0.0.0.0"
#endif
#ifndef REDIS_PORT
#define REDIS_PORT 6379
#endif
#ifndef PREFIX
#define PREFIX "challenge"
#endif

static void __attribute__((noreturn)) fatal(const char *fmt, ...);
static ssize_t read_all(int fd, char* ptr, size_t len);
static void hash_sha512(uint8_t* data, size_t data_len, uint8_t output[2*SHA512_DIGEST_LENGTH]);
static char to_hex(unsigned char n);

static void __attribute__((noreturn)) fatal(const char *fmt, ...) {
    va_list fmt_args;

    fprintf(stderr, "ERROR: ");
    va_start(fmt_args, fmt);
    if (vfprintf(stderr, fmt, fmt_args) < 0) {
        fprintf(stderr, " (also error in fatal) ");
    }
    va_end(fmt_args);
    fprintf(stderr, " Report to admins.\n");
    exit(EXIT_FAILURE);
}

static ssize_t read_all(int fd, char* ptr, size_t len) {
    ssize_t ret = 0;
    while (len > 0) {
        ssize_t r = read(fd, ptr, len);
        if (r < 0)
            return r;
        size_t r_ = (size_t)r;
        ret += r_;
        ptr += r_;
        len -= r_;
    }
    return ret;
}

static char to_hex(unsigned char n) {
    if (n < 10)
        return '0' + n;
    return 'a' + n - 10;
}

static void hash_sha512(uint8_t* data, size_t data_len, uint8_t output[2*SHA512_DIGEST_LENGTH]) {
    SHA512_CTX ctx;
    SHA512_Init(&ctx);
    SHA512_Update(&ctx, data, data_len);
    SHA512_Final(output, &ctx);
    for (size_t i = SHA512_DIGEST_LENGTH; i > 0; --i) {
        unsigned char c = output[i-1];
        output[2*i-2] = to_hex(c >> 4);
        output[2*i-1] = to_hex(c & 15);
    }
}

int main(int argc, char* argv[], char* envp[]) {
    char* challenge;
    char token[SIZE];
    uint8_t hash[2*SHA512_DIGEST_LENGTH];
    redisContext *redis;
    redisReply* reply;
    long long authenticated;

    if (argc <= 1)
        fatal("Path for challenge not provided.");

    if ((challenge = getenv("CHALLENGE_NAME")) == NULL)
        fatal("Cannot get challenge name.");
    memset(token, 0, sizeof(token));
    redis = redisConnect(REDIS_IP, REDIS_PORT);
    if (redis == NULL)
        fatal("Cannot allocate context.");
    if (redis->err)
        fatal("Redis error: %s.", redis->errstr);
    if (read_all(STDIN_FILENO, token, sizeof(token)) < 0)
        fatal("reading failed: %d(%s)", errno, strerror(errno));
    hash_sha512((uint8_t*)token, sizeof(token), hash);
    reply = redisCommand(
                redis, 
                "SISMEMBER %s:%s %b", 
                PREFIX,
                challenge, 
                hash, sizeof(hash));
    if (reply == NULL)
        fatal("Reading from redis failed: %s.", redis->errstr);
    if (reply->type != REDIS_REPLY_INTEGER)
        fatal("Redis returned wrong type (%d).", reply->type);
    authenticated = reply->integer;
    freeReplyObject(reply);
    redisFree(redis);
    if (!authenticated) {
        fprintf(stderr, "You do not have permission to access this challenge. Sorry :c\n");
        exit(EXIT_FAILURE);
    }
    if (execvpe(argv[1], &argv[1], envp) < 0)
        fatal("Exec failed with code %d(%s).", errno, strerror(errno));
}
